from django.contrib import admin

# Register your models here.
from .models import Persona, Contacto, Image





class AdminPersona(admin.ModelAdmin):
    list_display=["email","nombre","fechahora"]
    list_filter=["fechahora"]
    search_fields=["nombre"]



admin.site.register(Contacto)


admin.site.register(Image)




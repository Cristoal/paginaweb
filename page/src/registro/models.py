from __future__ import unicode_literals
from django.db import models


# Create your models here.
class Persona(models.Model):
    nombre=models.CharField(max_length=25, blank=True, null=True)
    email=models.EmailField()
    fechahora=models.DateTimeField(auto_now=False,auto_now_add=True)

    def __str__(self):
        return self.email

    #python 2
   #def __unicode__(self):
   #    return self.email

class Contacto(models.Model):
    nombre = models.CharField(max_length=50)
    correo = models.EmailField()
    mensaje = models.TextField()

    def __str__(self):
        return self.nombre



# Create your models here.
class Image(models.Model):
    caption=models.CharField(max_length=100)
    image=models.ImageField(upload_to="img/%y")
    def __str__(self):
        return self.caption



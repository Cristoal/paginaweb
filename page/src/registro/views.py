from django.shortcuts import render, redirect
from .forms import RegistroPersona
from django.contrib.auth.decorators import login_required
from .forms import CustomUserCreationForm
from .forms import ContactForm
from django.contrib import messages
from .forms import ImageForm
from .models import Image
# Create your views here.
@login_required
def index(request):
    #form=RegistroPersona()



    #context={
        #"formulario":form
        
    #}
    return render(request,"index.html",{})



def registro(request):
    form=CustomUserCreationForm(request.POST or None)

    context={
        "form":form

    }

    if form.is_valid():
        form.save()
        #return render(request,"index.html",{})
        
        return redirect(to='index.html')
    return render(request,"registration/registro.html",context)


def contact(request):
    data = {
        "formulario": ContactForm() 
    }

    if request.method == "POST":

        formulariodatos = ContactForm(data=request.POST)
        if formulariodatos.is_valid():
            
            formulariodatos.save()
            messages.success(request, "Tu mensaje sera revisado") 
            return redirect(to='contact.html')
            
        else:
            data["formulario"] = formulariodatos
            
    return render(request,"contact.html",data)


def click_image(request):
    return render(request,"click_image.html",{}) 


def subir(request):
    if request.method == "POST":
        form=ImageForm(data=request.POST,files=request.FILES)
        if form.is_valid():
            form.save()
            obj=form.instance
            return render(request,"subir.html",{"obj":obj})  
    else:
        form=ImageForm()
    img=Image.objects.all()
    return render(request,"subir.html",{"img":img,"form":form})

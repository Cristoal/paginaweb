from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Contacto
from .models import Image

class RegistroPersona(forms.Form):
    nombre=forms.CharField(max_length=25)
    email=forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Email'}))
    password=forms.CharField(widget=forms.PasswordInput())


class ContactForm(forms.ModelForm):

    class Meta:
        model = Contacto
        fields = ["nombre","correo","mensaje"]

class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    

    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2" ]


class ImageForm(forms.ModelForm):
    class Meta:
        model=Image
        fields=("caption","image")
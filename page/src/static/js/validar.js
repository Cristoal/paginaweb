function validar(){
    var email, nombre, apellido, mensaje, expresion, expresion2, expresion3;
    email = document.getElementById("email").value;
    nombre = document.getElementById("nombre").value;
    apellido = document.getElementById("apellido").value;
    mensaje = document.getElementById("mensaje").value;

    

    expresion = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
    expresion2 = /^([A-Za-zÁÉÍÓÚñáéíóúÑ]+)$/;
    expresion3 = /^([A-Za-zÁÉÍÓÚñáéíóúÑ]+)$/;



    if(nombre === "" || apellido === "" || email === "" || mensaje === ""){
        alert("Todos los campos son obligatorios");
        return false;

    }
    else if(nombre.length>30){
        alert("El nombre es muy largo");
        return false;
    }
    else if(!expresion2.test(nombre)){
        alert("El nombre no es valido");
        return false;
    }
    else if(apellido.length>30){
        alert("El apellido es muy largo");
        return false;
    }
    else if(!expresion3.test(apellido)){
        alert("El apellido no es valido");
        return false;
    }
    else if(email.length>100){
        alert("El correo es muy largo");
        return false;
    }
    else if(!expresion.test(email)){
        alert("El correo no es valido");
        return false;
    }
    else if(mensaje.length>300){
        alert("El mensaje es muy largo");
        return false;
    }
}
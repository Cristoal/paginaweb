function validarlogin(){
    var email_login, password_login, expresion_login, expresion2_login;
    email_login = document.getElementById("email_login").value;
    password_login = document.getElementById("password_login").value;

    

    expresion_login = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
    expresion2_login = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    



    if(email_login === "" || password_login === ""){
        alert("Todos los campos son obligatorios");
        return false;

    }
    else if(email_login.length>100){
        alert("El correo es muy largo");
        return false;
    }
    else if(!expresion_login.test(email_login)){
        alert("El correo no es valido");
        return false;
    }
    else if(password_login.length>300){
        alert("La Contraseña es muy larga");
        return false;
    }
    else if(!expresion2_login.test(password_login)){
        alert("Contraseña: Debe contener al menos un numero y una letra mayúscula y minúscula, y al menos 8 caracteres o más");
        return false;
    }
}
